console.log("hello world");
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var C = document.getElementById('sure');
var mode = document.getElementsByName('MODE');
var CC = document.getElementById('CHOOSE');
var M=0;
var drawtext = document.getElementById('textdraw');

var start = document.getElementById('startupload');
start.addEventListener('click',function(){
    console.log("Should be drawing")
    var img = new Image();
    img.src = document.getElementById('myfile');
    ctx.drawImage(img,0,0);
},false);

drawtext.addEventListener('click',function(){ 
    if(M==2){
        var R =document.getElementById('red').value;
        var G =document.getElementById('green').value;
        var B =document.getElementById('blue').value;
        ctx.globalCompositeOperation="source-over";
        ctx.font = document.getElementById('brushSize').value+"px Arial"
        ctx.fillStyle = "rgb("+R+","+G+","+B+")"
        ctx.fillText(document.getElementById('textinput').value,document.getElementById('textX').value,document.getElementById('textY').value);
    }},false)


function setcursor(){
    if(M==0){
        canvas.style.cursor = "crosshair";
    }
    else if(M==1){
        canvas.style.cursor = "url(https://img.icons8.com/ios/50/000000/erase-filled.png)5 25,auto";
    }
    else if(M==2){
        canvas.style.cursor = "url(https://img.icons8.com/metro/26/000000/t.png)5 25,auto"
    }
}
function ModeListen(){
for(var i=0;i<mode.length;i++){
    console.log(i);
    if(mode[i].checked){
        console.log(i+"is checked");
        M = i;
    }
    setcursor();
}}
CC.addEventListener('click',ModeListen,false);

function GetMousePos(canvas,evt){
    var rect = canvas.getBoundingClientRect();
    console.log(evt.clientX-rect.left,evt.clientY-rect.top);
    if(M==0)return{
        x: evt.clientX-rect.left,
        y: evt.clientY-rect.top
    }
    return{
        x: evt.clientX-rect.left+5,
        y: evt.clientY-rect.top+20
    };
}
function draw(evt){
    var mousePos = GetMousePos(canvas,evt);
    ctx.lineTo(mousePos.x,mousePos.y);
    ctx.globalCompositeOperation="source-over";
    ctx.strokeStyle =setColorandSize().color;
    ctx.lineWidth = setColorandSize().size;
    ctx.stroke();
}
function erase(evt){
    var mousePos = GetMousePos(canvas,evt);
    ctx.lineTo(mousePos.x,mousePos.y);
    ctx.globalCompositeOperation="destination-out";
    ctx.lineWidth = setColorandSize().size;
    ctx.stroke();
}
canvas.addEventListener('mousedown',function(evt){
    var mousePos = GetMousePos(canvas,evt);
    ctx.beginPath();
    ctx.moveTo(mousePos.x,mousePos.y);
    evt.preventDefault();
    if(M == 0){
        canvas.addEventListener('mousemove',draw,false);
    }
    else if(M == 1){
        canvas.addEventListener('mousemove',erase,false);
    }
    else if(M==2){
        document.getElementById('textX').value = mousePos.x;
        document.getElementById('textY').value = mousePos.y;
    }
});


canvas.addEventListener('mouseup',function(){
    canvas.removeEventListener('mousemove',draw,false);
},false);
canvas.addEventListener('mouseup',function(){
    canvas.removeEventListener('mousemove',erase,false);
},false);

function setColorandSize(){
var R =document.getElementById('red').value;
var G =document.getElementById('green').value;
var B =document.getElementById('blue').value;
var Size = document.getElementById('brushSize').value;
var Color = document.getElementById('COLOR').value;
console.log(R, G, B);
return {
    color:"rgb("+R+","+G+","+B+")",
    size: Size
}
}
C.addEventListener('click',setColorBycolorpicker,false);

function setColorBycolorpicker(){
var Color = document.getElementById('COLOR').value;
var Color1 = [Color[1],Color[2]].join('');
var Color2 = [Color[3],Color[4]].join('');
var Color3 = [Color[5],Color[6]].join('');
    document.getElementById('red').value = parseInt(Color1,16);
    document.getElementById('green').value = parseInt(Color2,16);
    document.getElementById('blue').value = parseInt(Color3,16);
    console.log([Color[1],Color[2]]);
    console.log(Color[0],Color[1],Color[2],Color[3],Color[4],Color[5],Color[6])
    console.log(Color);
    console.log(M);
}

var res = document.getElementById('reset');
res.addEventListener('click',function(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);