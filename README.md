## Put your report below here

利用html5和java script做出canvas。

畫筆的顏色粗細利用html input跟ctx.strokeStyle和ctx.lineWidth互動來完成。
顏色調配可用三個可拉的html range來各自調整，也可以使用html color元件來改，color元件改完必須要按change才會生效，range則是拖拉即有效。
畫筆cursor的設定利用canvas.style.cursor完成。
負責改變mode的ratio button一共有四種選項，在brush模式才可以畫畫，erase模式是橡皮擦，text模式是文字，shape模式是畫圖形，cursor style的改變是隨著mode一起改變的，同樣必須按下choose mode才會改變mode
brush模式跟erase模式都是偵測mousedown跟mousemove後，利用eventlistener實作出來的。
text模式下，可以利用滑鼠游標指定字的x,y座標，也可在input text裡直接輸入，按下done之後即會在指定位置輸入text。同樣listen mouse所在位置後將其輸入input格內。
text模式的預設值字太小且容易讓字在視窗外，請多加注意。
Reset的部分利用clearRect() function完成。
新增save canvas功能。
畫圖形的部分做到一半還沒做完。